import React, { useRef } from 'react'
import './assets/css/style.css'

// assets image
import banner from './assets/img/architect.jpg';
import house2 from './assets/img/house2.jpg';
import house3 from './assets/img/house3.jpg';
import house4 from './assets/img/house4.jpg';
import house5 from './assets/img/house5.jpg';
import map from './assets/img/map.jpg';
import team1 from './assets/img/team1.jpg';
import team2 from './assets/img/team2.jpg';
import team3 from './assets/img/team3.jpg';
import team4 from './assets/img/team4.jpg';

function App() {
  const projectsRef = useRef();
  const aboutRef = useRef();
  const contactRef = useRef();

  function handleNavClick() {
    projectsRef.current.scrollIntoView({ behavior: 'smooth' })
  }
  function handleAboutClick() {
    aboutRef.current.scrollIntoView({ behavior: 'smooth' })
  }
  function handleContactClick() {
    contactRef.current.scrollIntoView({ behavior: 'smooth' })
  }

  function handleSubmit(e) {
    e.preventDefault();
  }


  return (
    <div className="App">

      {/* Navbar (sit on top) */}
      <div className='navBar'>
        <a href="/#"><b>EDTS</b> TDP Batch #2</a>
        <div className='navBar_menu'>
          <button id="projects" onClick={handleNavClick}>Projects</button>
          <button id="about" onClick={handleAboutClick}>About</button>
          <button id="contact" onClick={handleContactClick}>Contact</button>
        </div>
      </div>

      {/* Header */}
      <header>
        <img src={banner} alt="Architecture" width={1500} height={800} />
        <div className='imageText'>
          <h1 id='edts'>
              EDTS 
          </h1>
          <h1 id='architects'>
            Architects
          </h1>
        </div>
      </header>

      {/* Page content */}
      <div className="container">

        {/* Project Section */}
        <div ref={projectsRef} id="projects" name="projects">
          <h3>Projects</h3>
        </div>
        <hr></hr>
        <div className='projectContainer'>
          <div className='project'>
            <div>Summer House</div>
            <img src={house2} alt="House" />
          </div>
          <div className='project'>
            <div>Brick House</div>
            <img src={house3} alt="House" />
          </div>
          <div className='project'>
            <div>Renovated</div>
            <img src={house4} alt="House" />
          </div>
          <div className='project'>
            <div >Barn House</div>
            <img src={house5} alt="House" />
          </div>
          <div className='project'>
            <div>Summer House</div>
            <img src={house3} alt="House" />
          </div>
          <div className='project'>
            <div>Brick House</div>
            <img src={house2} alt="House" />
          </div>
          <div className='project'>
            <div>Renovated</div>
            <img src={house5} alt="House" />
          </div>
          <div className='project'>
            <div>Barn House</div>
            <img src={house4} alt="House" />
          </div>
          

        </div>
        

        
        
        
        
        
        

        {/* About Section */}
        <div>
          <h3 ref={aboutRef} id="about">About</h3>
          <hr></hr>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Excepteur sint
            occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur
            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco
            laboris nisi ut aliquip ex ea commodo consequat.
          </p>
        </div>
        <div className='founderProfiles'>
          <div className='founderProfile'>
            <img src={team1} alt="Jane" />
            <h3>Jane Doe</h3>
            <p className='job'>CEO &amp; Founder</p>
            <p className='founderDescription'>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <button>Contact</button>
          </div>
          <div className='founderProfile'>
            <img src={team2} alt="John" />
            <h3>John Doe</h3>
            <p className='job'>Architect</p>
            <p className='founderDescription'>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <button>Contact</button>
          </div>
          <div className='founderProfile'>
            <img src={team3} alt="Mike" />
            <h3>Mike Ross</h3>
            <p className='job'>Architect</p>
            <p className='founderDescription'>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <button>Contact</button>
          </div>
          <div className='founderProfile'>
            <img src={team4} alt="Dan" />
            <h3>Dan Star</h3>
            <p className='job'>Architect</p>
            <p className='founderDescription'>Phasellus eget enim eu lectus faucibus vestibulum. Suspendisse sodales pellentesque elementum.</p>
            <button>Contact</button>
          </div>
        </div>

        {/* Contact Section */}
        <div ref={contactRef} id="contact">
          <h3>Contact</h3>
          <hr></hr>
          <p>Lets get in touch and talk about your next project.</p>
          <form onSubmit={handleSubmit}>
            <input className="inputForm" type="text" id="name" name="name" placeholder="Name" />
            <input className="inputForm" type="email" id="email" name="email" placeholder="Email" />
            <input className="inputForm" type="text" id="subject" name="subject" placeholder="Subject" />
            <input className="inputForm" type="text" id="comment" name="comment" placeholder="Comment" />
            <input className="buttonForm" type="submit" value="SEND MESSAGE" />
          </form>

          {/***  
           add your element form here 
           ***/}

        </div>

        {/* Image of location/map */}
        <div className='map' >
          <img src={map} alt="maps" />
        </div>

        <div>
          <h3>Contact List</h3>
          <hr></hr>
          {/***  
           add your element table here 
           ***/}
           <table>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Country</th>
              </tr>
              <tr>
                <td>Alfreds Futterkiste</td>
                <td>alfreds@gmail.com</td>
                <td>Germany</td>
              </tr>
              <tr>
                <td>Reza</td>
                <td>reza@gmail.com</td>
                <td>Indonesia</td>
              </tr>
              <tr>
                <td>Ismail</td>
                <td>ismail@gmail.com</td>
                <td>Turkey</td>
              </tr>
              <tr>
                <td>Holand</td>
                <td>holand@gmail.com</td>
                <td>Belanda</td>
              </tr>
            </table>

        </div>

        {/* End page content */}
      </div>

      {/* Footer */}
      <footer>
        <p>Copyright 2022</p>
      </footer>

    </div>
  );
}

export default App;
